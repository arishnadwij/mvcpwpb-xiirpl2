<?php
    class Blog_model 
    {
        private $table = 'blog';
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function getAllBlog() {
            $this->db->query("SELECT * FROM {$this->table}");

            return $this->db->resultAll();
        }

        public function getBlogById($id)
        {
            $this->db->query("SELECT * FROM $this->table WHERE id = :id");
            $this->db->bind('id', $id);

            return $this->db->resultSingle();
        }

        public function tambahDataBlog($data)
        {
            $query = "INSERT INTO blog (Penulis, Judul, Tulisan)
            VALUES
          ( :Penulis, :Judul, :Tulisan)";

                $this->db->query($query);
                $this->db->bind('Penulis', $data['Penulis']);
                $this->db->bind('Judul', $data['Judul']);
                $this->db->bind('Tulisan', $data['Tulisan']);

                $this->db->execute();

                return $this->db->rowCount();
        }

        public function hapusDataBlog($id)
        {
            $query = "DELETE FROM blog WHERE id = :id";
            $this->db->query($query);
            $this->db->bind('id', $id);

            $this->db->execute();
            
            return $this->db->rowCount();
        }

        public function ubahDataBlog($data)
        {
            $query = "UPDATE blog SET
                         Penulis = :Penulis,
                         Judul = :Judul,  
                         Tulisan = :Tulisan
                         WHERE id = :id";

                $this->db->query($query);
                $this->db->bind('Penulis', $data['Penulis']);
                $this->db->bind('Judul', $data['Judul']);
                $this->db->bind('Tulisan', $data['Tulisan']);
                $this->db->bind('id', $data['id']);

                $this->db->execute();

                return $this->db->rowCount();
        }

        public function cariDataBlog()
        {
            $keyword = $_POST['keyword'];
            $query = "SELECT * FROM blog WHERE Judul LIKE :keyword";
            $this->db->query($query);
            $this->db->bind('keyword', "%$keyword%");

            return $this->db->resultSet();
        }

    }
?>