<?php
   class Home extends Controller {
    public function index() {
        // echo "Home/index";
        $data["Judul"] = "Home";
        $data["Username"] = "Arishna";
        $this->view("templates/header", $data);
        $this->view("home/index", $data);
        $this->view("templates/footer");
    }
}
?>