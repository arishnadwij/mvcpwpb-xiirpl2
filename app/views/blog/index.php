<div class="container mt-5">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-6">
            <button type="button" class="btn btn-primary tombolTambahData" data-bs-toggle="modal"data-bs-target="#formModal">
                 Tambah Data Blog
            </button>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-lg-6">
            <form action="<?= BASE_URL; ?>/blog/cari" method="post">
             <div class="input-group">
                <input type="text" class="form-control" placeholder="Cari blog..." name="keyword" id="keyword" autocomplete="off">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="tombolCari">Cari</button>
                    </div>
                </div>
           </form>
        </div>
    </div>

   <div class="row">
       <div class="col-lg-6">
           <h3>Data Blog</h3>
           <ul class="list-group">
           <?php foreach($data["blog"] as $blog) :?>
                <li class="list-group-item">
                    <?=$blog['Judul']; ?>

                    <a href="<?=BASE_URL;?>/blog/detail/<?= $blog['id']?>"
                    class="badge bg-primary">Detail</a>
                    
                    
                    <a href="<?=BASE_URL;?>/blog/ubah/<?= $blog['id']?>"
                    class="badge bg-success float-right tampilModalUbah" data-bs-toggle="modal"data-bs-target="#formModal" 
                    data-id="<?=$blog['id']; ?>">Ubah</a>

                    <a href="<?=BASE_URL;?>/blog/hapus/<?= $blog['id']?>"
                    class="badge bg-danger float-right"
                    onclick="return confirm('yakin?');">Hapus</a>
                </li>
                <?php endforeach; ?>
           </ul>
       </div>
   </div>
</div>


<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel">Tambah Data Blog</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

    <div class="modal-body">
        <form action="<?= BASE_URL; ?>/blog/tambah" method="post">
        <input type="hidden" name="id" id="id">
            <div class="form-group">
                        <label for="Penulis">Penulis</label>
                        <input type="text" class="form-control" id="Penulis" name="Penulis">
                    
                        <label for="Judul">Judul</label>
                        <input type="text" class="form-control" id="Judul" name="Judul">
                
                        <label for="Tulisan">Tulisan</label>
                        <input type="text" class="form-control" id="Tulisan" name="Tulisan">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
                </form>  
            </div>
        </div>
    </div>
</div>