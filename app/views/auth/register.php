<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="body"> 

	<form action="" method="post">
		<div class="card-regis">
        <center><h1>REGISTER</h1></center>
			<div class="form">
				<label for="username">Username</label>
				<input type="text" name="username" id="username" placeholder="Masukkan Username Anda">

				<label for="email">Email</label>
				<input type="text" name="email" id="email" placeholder="Masukkan Email Anda">

				<label for="password">Password</label>
				<input type="password" name="password" id="password" placeholder="Masukkan Password Anda">

				<center>
					<button type="submit" name="register" class="button-logregis">Submit</button>
					<button type="reset" class="button-logregis">Cancel</button><br>
					<a href="login.php">Already Have an Account ?</a>
				</center>
			</div>
		</div>
	</form>

</body>
</html>